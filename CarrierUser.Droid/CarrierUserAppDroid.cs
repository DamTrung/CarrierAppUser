﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CarrierUser.Droid.Util;
using CarrierUserApp;
using CarrierUserApp.Models;

namespace CarrierUserApp.Droid
{
    [Application]
    public class CarrierUserAppDroid : Application
    {
        public CarrierUserAppDroid(IntPtr handle, JniHandleOwnership transfer)
            : base(handle, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            DataStorage dataStorage = new DataStorage(FileAccessHelper.GetRootFolderPath());
        }
        public static LoginInfo loginInfo { get; set; }
        public static LoginInfo CurrentLoginInfo()
        {
            DataStorage dataStorage = new DataStorage(FileAccessHelper.GetRootFolderPath());
            return dataStorage.ReadData<LoginInfo>("LoginInfo", true);
        }
        public static string DeviceToken()
        {
            DataStorage dataStorage = new DataStorage(FileAccessHelper.GetRootFolderPath());
            return dataStorage.ReadData<string>("DeviceToken");
        }
    }
}