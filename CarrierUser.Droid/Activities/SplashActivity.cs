﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using CarrierUser.Droid.Util;
using CarrierUserApp;
using CarrierUserApp.Droid;
using CarrierUserApp.Models;

namespace CarrierUser.Droid.Activities
{
    [Activity(MainLauncher = true, Theme = "@style/CarrierUserTheme.Splash", NoHistory = true,Icon ="@drawable/icon_carrier")]
    public class SplashActivity : AppCompatActivity
    {
        static readonly string TAG = "X:" + typeof(SplashActivity).Name;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        void goHomeScreen()
        {
            var intent = new Intent(Application.Context, typeof(MainActivity));
            StartActivity(intent);
        }
        void goLoginScreen()
        {
            var intent = new Intent(Application.Context, typeof(LoginActivity));
            StartActivity(intent);
        }
        void goListOrder()
        {
            var intent = new Intent(Application.Context, typeof(ListOrderActivity));
            StartActivity(intent);
        }
        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { SimulateStartup(); });
            startupWork.Start();
        }

        // Prevent the back button from canceling the startup process
        public override void OnBackPressed() { }

        // Simulates background work that happens behind the splash screen
        async void SimulateStartup()
        {
            //goListOrder();
            goLoginScreen();
            //if (CarrierUserAppDroid.loginInfo != null)
            //{
            //    goHomeScreen();
            //}
            //else
            //{
            //    var dataStorage = new DataStorage(FileAccessHelper.GetRootFolderPath());
            //    var loginInfo = dataStorage.ReadData<LoginInfo>("LoginInfo");
            //    CarrierUserAppDroid.loginInfo = loginInfo;
            //    if (loginInfo.CurrentToken != null)
            //    {
            //        goHomeScreen();
            //    }
            //    else
            //    {
            //        goLoginScreen();
            //    }
            //}
        }
    }
}