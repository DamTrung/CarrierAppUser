﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using AndroidHUD;
using CarrierUser.Droid.Helpers;
using CarrierUserApp;
using CarrierUserApp.Droid;
using CarrierUserApp.Models;
using Newtonsoft.Json;
using Fragment = Android.Support.V4.App.Fragment;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "ListOrderActivity")]
    public class ListOrderActivity : Activity
    {
        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        RecyclerViewAdapter mAdapter;
        DataService service;
        SwipeRefreshLayout refreshLayout;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                base.OnCreate(savedInstanceState);
                SetContentView(Resource.Layout.List_Title);

                var toolbar = FindViewById<Toolbar>(Resource.Id.app_bar_List);

                var btnBack = toolbar.FindViewById<ImageView>(Resource.Id.btnBack_List);
                btnBack.Click += goHome;

                mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);

                refreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);
                refreshLayout.SetColorSchemeColors(Resource.Color.primaryDark);
                refreshLayout.Refresh += RefreshLayout_Refresh;

                service = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);
                var lstOrderRefresh = await service.GetListOrder();

                mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
                mRecyclerView.SetLayoutManager(mLayoutManager);

                mAdapter = new RecyclerViewAdapter(lstOrderRefresh.Result);

                mAdapter.ItemClick += Adapter_ItemClick;
                mRecyclerView.SetAdapter(mAdapter);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void RefreshLayout_Refresh(object sender, EventArgs e)
        {
            Task.Run(async () =>
            {
                service = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);
                var lstOrderRefresh = await service.GetListOrder();
                mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
                mRecyclerView.SetLayoutManager(mLayoutManager);

                mAdapter = new RecyclerViewAdapter(lstOrderRefresh.Result);

                mAdapter.ItemClick += Adapter_ItemClick;
                mRecyclerView.SetAdapter(mAdapter);
            });
        }

        void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            var item = mAdapter.lstData[e.Position];
            var intent = new Intent(this, typeof(DetailActivity));
            intent.PutExtra("order_detail", JsonConvert.SerializeObject(item));
            StartActivity(intent);
        }

        void goHome(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
    }
}