﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using AndroidHUD;
using CarrierUser.Droid.Fragments;
using CarrierUser.Droid.Util;
using CarrierUserApp;
using CarrierUserApp.Droid;
using CarrierUserApp.Models;
using CarrierUserApp.Utils;
using Newtonsoft.Json;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "OrderActivity")]
    public class OrderActivity : Activity, IOnMapReadyCallback
    {

        public TextView datePickerFrom;
        public TextView datePickerTo;
        public TextView timePickerFrom;
        public TextView timePickerTo;
        public TextView viewDateFrom;
        public TextView viewDateTo;
        public TextView viewTimeFrom;
        public TextView viewTimeTo;
        public EditText phone;

        public DateTime currentDate = DateTime.Now;
        //DatabaseHelper database;
        public const int EquatorRadius = 6378137;

        private AutoCompleteTextView diemdi;
        private AutoCompleteTextView diemden;
        const string strAutoCompleteGoogleApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=";
        //const string strGoogleApiKey = "AIzaSyC7-DFM3nHBHRZlC_V2BJG4syP2sb2_it4";
        const string strGoogleApiKey = "AIzaSyCR2kBa7TNKIOf3BELdxWyL5npuuJ_YuUM";
        const string strGeoCodingUrl = "https://maps.googleapis.com/maps/api/geocode/json";
        const string urlLocations = "http://maps.googleapis.com/maps/api/directions/json?";
        //MapFragment mapFrag;
        //GoogleMap map;
        ArrayAdapter adapter = null;
        GoogleMapPlaceClass objMapClass;
        GeoCodeJSONClass objGeoCodeJSONClassDiemDi, objGeoCodeJSONClassDiemDen;
        string autoCompleteOptions;
        string[] strPredictiveText;
        int index = 0;
        string[] words;
        double km = 0;
        double price = 0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Order);
            var btnBack = FindViewById<ImageView>(Resource.Id.btnBack_Order);
            datePickerFrom = FindViewById<TextView>(Resource.Id.TxtDateFrom);
            datePickerFrom.Text = CarrierUserApp.Utils.Util.ShortDateFromString(DateTime.Now);
            timePickerFrom = FindViewById<TextView>(Resource.Id.TxtTimeFrom);
            timePickerFrom.Text = CarrierUserApp.Utils.Util.ShortTimeString(DateTime.Now);

            diemdi = FindViewById<AutoCompleteTextView>(Resource.Id.txtDiemDi);
            diemden = FindViewById<AutoCompleteTextView>(Resource.Id.txtDiemDen);
            diemdi.ItemClick += AutoCompleteOption_Click;
            diemdi.Hint = "Nhập điểm đi";
            diemden.ItemClick += AutoCompleteOption_ClickDiemDen;
            diemden.Hint = "Nhập điểm đến";

            diemdi.TextChanged += async delegate (object sender, Android.Text.TextChangedEventArgs e)
            {
                try
                {
                    autoCompleteOptions = await fnDownloadString(strAutoCompleteGoogleApi + diemdi.Text + "&key=" + strGoogleApiKey);

                    if (autoCompleteOptions == "Exception")
                    {
                        Toast.MakeText(this, "Unable to connect to server!!!", ToastLength.Short).Show();
                        return;
                    }
                    objMapClass = JsonConvert.DeserializeObject<GoogleMapPlaceClass>(autoCompleteOptions);
                    strPredictiveText = new string[objMapClass.predictions.Count];
                    index = 0;
                    foreach (Prediction objPred in objMapClass.predictions)
                    {
                        strPredictiveText[index] = objPred.description;
                        index++;
                    }
                    adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleDropDownItem1Line, strPredictiveText);
                    diemdi.Adapter = adapter;
                }
                catch
                {
                    Toast.MakeText(this, "Unable to process at this moment!!!", ToastLength.Short).Show();
                }

            };

            diemden.TextChanged += async delegate (object sender, Android.Text.TextChangedEventArgs e)
            {
                try
                {
                    autoCompleteOptions = await fnDownloadString(strAutoCompleteGoogleApi + diemden.Text + "&key=" + strGoogleApiKey);

                    if (autoCompleteOptions == "Exception")
                    {
                        Toast.MakeText(this, "Unable to connect to server!!!", ToastLength.Short).Show();
                        return;
                    }
                    objMapClass = JsonConvert.DeserializeObject<GoogleMapPlaceClass>(autoCompleteOptions);
                    strPredictiveText = new string[objMapClass.predictions.Count];
                    index = 0;
                    foreach (Prediction objPred in objMapClass.predictions)
                    {
                        strPredictiveText[index] = objPred.description;
                        index++;
                    }
                    adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleDropDownItem1Line, strPredictiveText);
                    diemden.Adapter = adapter;
                }
                catch
                {
                    Toast.MakeText(this, "Unable to process at this moment!!!", ToastLength.Short).Show();
                }

            };

            datePickerTo = FindViewById<TextView>(Resource.Id.txtDateTo);
            datePickerTo.Text = CarrierUserApp.Utils.Util.ShortDateFromString(DateTime.Now);
            timePickerTo = FindViewById<TextView>(Resource.Id.txtTimeTo);
            timePickerTo.Text = CarrierUserApp.Utils.Util.ShortTimeString(DateTime.Now);

            viewDateFrom = FindViewById<TextView>(Resource.Id.viewFromDate);
            viewDateTo = FindViewById<TextView>(Resource.Id.viewToDate);
            viewTimeFrom = FindViewById<TextView>(Resource.Id.viewFromTime);
            viewTimeTo = FindViewById<TextView>(Resource.Id.viewToTime);

            viewDateFrom.Click += DatePicker_Click;
            viewDateTo.Click += DatePickerTo_Click;
            viewTimeFrom.Click += FromTimePicker_Click;
            viewTimeTo.Click += ToTimePicker_Click;


            if (diemdi.Text != "" && diemden.Text != "")
            {
                double lat1, lng1, lat2, lng2;
                lat1 = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lat;
                lng1 = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lng;
                lat2 = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lat;
                lng2 = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lng;
                double distance = GetDistance(lat1, lng1, lat2, lng2, 'K');
                var soKmUocTinh = FindViewById<TextView>(Resource.Id.txtKmuoctinh);
                soKmUocTinh.Text = distance.ToString();
            }

            phone = FindViewById<EditText>(Resource.Id.TxtPhonenumber);
            var nguoilienhe = FindViewById<TextView>(Resource.Id.TxtUserContact);
            var ghichu = FindViewById<EditText>(Resource.Id.txtNote);
            var tenhang = FindViewById<EditText>(Resource.Id.TxtItems);
            var od_length = FindViewById<EditText>(Resource.Id.txtLenght);
            var od_height = FindViewById<EditText>(Resource.Id.txtHeight);
            var od_width = FindViewById<EditText>(Resource.Id.txtWidth);
            var carType = FindViewById<Spinner>(Resource.Id.CarType);
            phone.Text = "01683906798";
            nguoilienhe.Text = "Mr Tú";
            ghichu.Text = "Hàng cẩn thận";
            tenhang.Text = "Nguyên liệu tôn";
            od_height.Text = "3";
            od_length.Text = "2";
            od_width.Text = "3";

            //phone.AfterTextChanged += EditText_AfterTextChanged;

            words = carType.SelectedItem.ToString().Split(' ');
            var btnCreateOrder = FindViewById<Button>(Resource.Id.btnOrder);
            string strphone = string.Empty;
            btnCreateOrder.Click += async delegate
            {
                AndHUD.Shared.Show(this, "verifying...", -1, MaskType.Black);
                DataService dataService = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);
                //var service = new DataService(RovaPushApp.loginInfo.auth.auth_token);

                if (!Utitlitys.PhoneValidate(phone.Text, out strphone))
                {
                    AndHUD.Shared.Dismiss(this);
                    phone.Error = "Số điện thoại không đúng";
                    return;
                }

                OrderModel viewModel = new OrderModel();
                string thoigiandi = datePickerFrom.Text + " " + timePickerFrom.Text;
                string thoigianden = datePickerTo.Text + " " + timePickerTo.Text;
                viewModel.ThoiGianDi = thoigiandi;
                viewModel.ThoiGianDen = thoigianden;
                viewModel.Gia = price;
                viewModel.CarType = words[0];
                viewModel.TenHang = tenhang.Text;
                viewModel.Phone = phone.Text;
                viewModel.SoKmUocTinh = float.Parse(km.ToString());
                viewModel.Note = ghichu.Text;
                viewModel.DiemDiChiTiet = diemdi.Text;
                viewModel.DiemDenChiTiet = diemden.Text;
                viewModel.Lenght = float.Parse(od_height.Text.ToString());
                viewModel.Height = float.Parse(od_length.Text.ToString());
                viewModel.Width = float.Parse(od_width.Text.ToString());
                if (objGeoCodeJSONClassDiemDi != null)
                {
                    viewModel.FromLatitude = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lat;
                    viewModel.FromLongitude = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lng;
                }
                else
                {
                    viewModel.FromLatitude = 0;
                    viewModel.FromLongitude = 0;
                }
                if (objGeoCodeJSONClassDiemDen != null)
                {
                    viewModel.ToLatitude = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lat;
                    viewModel.ToLongitude = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lng;
                }
                else
                {
                    viewModel.ToLatitude = 0;
                    viewModel.ToLongitude = 0;
                }
                var createOrder = await dataService.AddOrder(viewModel);
                if (createOrder != null)
                {
                    if (createOrder.Message.Equals("Successfully"))
                    {
                        Toast.MakeText(this, "Tạo đơn thành công", ToastLength.Long).Show();
                    }
                    else
                    {
                        Toast.MakeText(this, createOrder.Message, ToastLength.Long).Show();
                    }
                }
                AndHUD.Shared.Dismiss(this);
            };
            btnBack.Click += async delegate
            {
                backHome();
            };
        }


        void DatePicker_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                datePickerFrom.Text = CarrierUserApp.Utils.Util.ShortDateFromString(time);
                currentDate = time;
            });
            frag.currently = currentDate;
            frag.Show(FragmentManager, DatePickerFragment.TAG);
        }
        void DatePickerTo_Click(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                datePickerTo.Text = CarrierUserApp.Utils.Util.ShortDateFromString(time);
                currentDate = time;
            });
            frag.currently = currentDate;
            frag.Show(FragmentManager, DatePickerFragment.TAG);
        }
        void FromTimePicker_Click(object sender, EventArgs eventArgs)
        {
            TimePickerFragment frag = TimePickerFragment.NewInstance(
                delegate (DateTime time)
                {
                    timePickerFrom.Text = time.ToShortTimeString();
                });

            frag.Show(FragmentManager, TimePickerFragment.TAG);
        }
        void ToTimePicker_Click(object sender, EventArgs eventArgs)
        {
            TimePickerFragment frag = TimePickerFragment.NewInstance(
                delegate (DateTime time)
                {
                    timePickerTo.Text = time.ToShortTimeString();
                });

            frag.Show(FragmentManager, TimePickerFragment.TAG);
        }
        async void AutoCompleteOption_Click(object sender, AdapterView.ItemClickEventArgs e)
        {
            //to soft keyboard hide
            InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
            inputManager.HideSoftInputFromWindow(diemdi.WindowToken, HideSoftInputFlags.NotAlways);

            if (diemdi.Text != string.Empty)
            {
                var sb = new StringBuilder();
                sb.Append(strGeoCodingUrl);
                sb.Append("?address=").Append(diemdi.Text);
                string strResult = await fnDownloadString(sb.ToString());
                if (strResult == "Exception")
                {
                    Toast.MakeText(this, "Unable to connect to server!!!", ToastLength.Short).Show();

                }
                //below used single quote to avoid html interpretation
                objGeoCodeJSONClassDiemDi = JsonConvert.DeserializeObject<GeoCodeJSONClass>(strResult);
                LatLng Position = new LatLng(objGeoCodeJSONClassDiemDi.results[0].geometry.location.lat, objGeoCodeJSONClassDiemDi.results[0].geometry.location.lng);
                //updateCameraPosition(Position);
                //MarkOnMap("MyLocation", Position);
                if (diemdi.Text != "" && diemden.Text != "")
                {
                    double lat1, lng1, lat2, lng2;
                    lat1 = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lat;
                    lng1 = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lng;
                    lat2 = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lat;
                    lng2 = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lng;
                    double distance = GetDistance(lat1, lng1, lat2, lng2, 'K');
                    var soKmUocTinh = FindViewById<TextView>(Resource.Id.txtKmuoctinh);
                    soKmUocTinh.Text = distance.ToString();
                    //AndHUD.Shared.Show(this, "Tính giá...", -1, MaskType.Black);
                    //double gia = 
                    //AndHUD.Shared.Dismiss(this);
                }
            }
        }
        async void AutoCompleteOption_ClickDiemDen(object sender, AdapterView.ItemClickEventArgs e)
        {
            //to soft keyboard hide
            InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
            inputManager.HideSoftInputFromWindow(diemden.WindowToken, HideSoftInputFlags.NotAlways);

            if (diemden.Text != string.Empty)
            {
                var sb = new StringBuilder();
                sb.Append(strGeoCodingUrl);
                sb.Append("?address=").Append(diemden.Text);
                string strResult = await fnDownloadString(sb.ToString());
                if (strResult == "Exception")
                {
                    Toast.MakeText(this, "Unable to connect to server!!!", ToastLength.Short).Show();

                }
                //below used single quote to avoid html interpretation
                objGeoCodeJSONClassDiemDen = JsonConvert.DeserializeObject<GeoCodeJSONClass>(strResult);
                LatLng Position = new LatLng(objGeoCodeJSONClassDiemDen.results[0].geometry.location.lat, objGeoCodeJSONClassDiemDen.results[0].geometry.location.lng);
                if (diemdi.Text != "" && diemden.Text != "")
                {
                    double lat1, lng1, lat2, lng2;
                    lat1 = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lat;
                    lng1 = (float)objGeoCodeJSONClassDiemDi.results[0].geometry.location.lng;
                    lat2 = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lat;
                    lng2 = (float)objGeoCodeJSONClassDiemDen.results[0].geometry.location.lng;

                    string lt1 = lat1.ToString("G", CultureInfo.InvariantCulture);
                    string lg1 = lng1.ToString("G", CultureInfo.InvariantCulture);
                    string lt2 = lat2.ToString("G", CultureInfo.InvariantCulture);
                    string lg2 = lng2.ToString("G", CultureInfo.InvariantCulture);

                    var soKmUocTinh = FindViewById<TextView>(Resource.Id.txtKmuoctinh);
                    var txtgio = FindViewById<TextView>(Resource.Id.txtSoGio);

                    var uri = urlLocations
                            + "origin=" + lt1 + "," + lg1
                            + "&destination=" + lt2 + "," + lg2
                            + "&sensor=true&units=metric";
                    string result;
                    result = await CallStringURL(uri);
                    RootObject json;
                    json = JsonConvert.DeserializeObject<RootObject>(result);
                    if (json.status.Equals("OK"))
                    {
                        var soKMUoc = json.routes[0].legs[0].distance.value;
                        var thoigian = json.routes[0].legs[0].duration.value;
                        var textthoigian = json.routes[0].legs[0].duration.text;
                        var cartype = FindViewById<Spinner>(Resource.Id.CarType);
                        km = soKMUoc / 1000.0;
                        words = cartype.SelectedItem.ToString().Split(' ');

                        txtgio.Text = textthoigian.ToString();
                        soKmUocTinh.Text = km.ToString();
                        var address = diemdi.Text + "|" + diemden.Text;
                        var strlat = lat1 + "|" + lat2;
                        var strlng = lng1 + "|" + lng2;
                        var p = getPrice(address, address, address, strlat, strlng, words[0], soKMUoc.ToString(), soKMUoc.ToString(), thoigian.ToString(), thoigian.ToString(), thoigian.ToString());

                    }
                    else
                    {
                        Toast.MakeText(this, "Lỗi tính Km", ToastLength.Short).Show();
                        txtgio.Text = "0";
                        soKmUocTinh.Text = "0";
                    }
                }
            }

        }
        void MarkOnMap(string title, LatLng pos)
        {
            RunOnUiThread(() =>
            {
                var marker = new MarkerOptions();
                marker.SetTitle(title);
                marker.SetPosition(pos);
                //map.AddMarker(marker);
            });

        }
        void updateCameraPosition(LatLng pos)
        {
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(pos);
            builder.Zoom(14);
            builder.Bearing(45);
            builder.Tilt(90);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            //map.AnimateCamera(cameraUpdate);
        }
        async Task<string> fnDownloadString(string strUri)
        {
            WebClient webclient = new WebClient();
            string strResultData;
            try
            {
                Uri address = new Uri(strUri);
                strResultData = await webclient.DownloadStringTaskAsync(address);
                //strResultData = await webclient.DownloadStringTaskAsync(strUri);
                Console.WriteLine(strResultData);
            }
            catch
            {
                strResultData = "Exception";
                RunOnUiThread(() =>
                {
                    Toast.MakeText(this, "Unable to connect to server!!!", ToastLength.Short).Show();
                });
            }
            finally
            {
                webclient.Dispose();
                webclient = null;
            }

            return strResultData;
        }
        async Task<string> CallStringURL(string strUri)
        {
            WebClient webclient = new WebClient();
            string strResultData;
            try
            {
                Uri address = new Uri(strUri);
                strResultData = await webclient.DownloadStringTaskAsync(address);
                Console.WriteLine(strResultData);
            }
            catch (Exception)
            {
                strResultData = "Exception";
                RunOnUiThread(() =>
                {
                    Toast.MakeText(this, "Unable to connect to server!!!", ToastLength.Short).Show();
                });
            }
            finally
            {
                webclient.Dispose();
                webclient = null;
            }

            return strResultData;
        }
        public void OnMapReady(GoogleMap googleMap)
        {
            throw new NotImplementedException();
        }
        private double GetDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = 0;
            dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (Math.Round(dist, 2));
        }
        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        void backHome()
        {
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
        async Task<double> getPrice(string addresssug, string addressApi, string address, string lat, string lng, string strOrderCar, string newdistance, string origirndistance, string newtime, string origirntime, string arrayDistance)
        {
            DataService dataService = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);
            var ordermodel = new ParameterGetPrice();
            ordermodel.address = address;
            ordermodel.addressApi = addressApi;
            ordermodel.addresssug = addresssug;
            ordermodel.arrayDistance = arrayDistance;
            ordermodel.lat = lat;
            ordermodel.lng = lng;
            ordermodel.newdistance = newdistance;
            ordermodel.newtime = newtime;
            ordermodel.origirndistance = origirndistance;
            ordermodel.origirntime = origirntime;
            ordermodel.strOrderCar = strOrderCar;
            var result = await dataService.GetPrice(ordermodel);
            if (result != null)
            {
                price = result.Result;
            }
            var cost = FindViewById<TextView>(Resource.Id.txtCost);
            cost.Text = Utitlitys.ConvertToCurrency(price);
            return price;
        }
        private void EditText_AfterTextChanged(object sender, AfterTextChangedEventArgs e)
        {
            var text = e.Editable.ToString();
            phone.AfterTextChanged -= EditText_AfterTextChanged;
            //var formatedText = PhoneValidate(text);
            string nbphone = phone.Text;
            string strphone = string.Empty;
            if (!Utitlitys.PhoneValidate(nbphone,out strphone))
            {
                Toast.MakeText(this, "Số điện thoại không đúng !!!", ToastLength.Short).Show();
                phone.AfterTextChanged += EditText_AfterTextChanged;
            }
            
        }
    }
}