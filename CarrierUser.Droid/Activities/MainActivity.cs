﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using CarrierUserApp.Models;
using Firebase;
using Newtonsoft.Json;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Support.V7.Widget;
using CarrierUser.Droid.Helpers;
using CarrierUserApp;
using CarrierUserApp.Droid;
using AndroidHUD;
using System.Threading.Tasks;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "CarrierUser", Icon = "@drawable/icon_carrier")]
    public class MainActivity : AppCompatActivity, IOnMapReadyCallback, GoogleMap.IInfoWindowAdapter, GoogleMap.IOnInfoWindowClickListener, NavigationView.IOnNavigationItemSelectedListener
    {
        private GoogleMap GMap;

        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        RecyclerViewAdapter mAdapter;
        //DatabaseHelper database;
        DataService service;
        //SwipeRefreshLayout refreshLayout;
        //LayoutInflater inflater;
        DriverAPI lstResult = new DriverAPI();
        string txtS = "";
        public DrawerLayout DrawerLayout { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            //base.OnCreate(savedInstanceState);
            //SetContentView(Resource.Layout.Main);

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            //SetSupportActionBar(toolbar);

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            Android.Support.V7.App.ActionBarDrawerToggle toggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);




            //var createOrder = FindViewById<TextView>(Resource.Id.itemOrder);
            //var listOrder = FindViewById<TextView>(Resource.Id.itemHistory);
            //var itemPolicy = FindViewById<TextView>(Resource.Id.itemPolicy);
            //createOrder.Click += goOrder;
            //listOrder.Click += goListOrder;
            //itemPolicy.Click += async delegate
            //{
            //    var intent = new Intent(this, typeof(AcceptOrderActivity));
            //    StartActivity(intent);
            //    Finish();
            //};
            SetUpMap();
            var txtSearch = FindViewById<TextView>(Resource.Id.txtSearch);
            service = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);

            SetUpMap();

            //txtSearch.KeyPress += async (object sender, View.KeyEventArgs e) =>
            //{
            //    e.Handled = false;
            //    if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            //    {
            //        txtS = txtSearch.Text;
            //        lstResult = await service.GetListDriver(txtS, txtS);

            //        e.Handled = true;
            //    }
            //};
        }
        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        //public override bool OnCreateOptionsMenu(IMenu menu)
        //{
        //    MenuInflater.Inflate(Resource.Menu.menu_main, menu);
        //    return true;
        //}

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        private void SetUpMap()
        {
            if (GMap == null)
            {
                FragmentManager.FindFragmentById<MapFragment>(Resource.Id.map).GetMapAsync(this);
            }
        }
        public async void OnMapReady(GoogleMap googleMap)
        {
            this.GMap = googleMap;
            var txtSearch = FindViewById<TextView>(Resource.Id.txtSearch);
            //service = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);

            LatLng latlng = new LatLng(Convert.ToDouble(21.027764), Convert.ToDouble(105.834160));
            CameraUpdate camera = CameraUpdateFactory.NewLatLngZoom(latlng, 12);
            GMap.MoveCamera(camera);
            txtS = txtSearch.Text;
            lstResult = await service.GetListDriver(txtS, txtS);
            //MarkerOptions options = new MarkerOptions().SetPosition(latlng).SetTitle("Carrier");
            var lstDriver = lstResult.Result.drivers;
            List<MarkerOptions> options = new List<MarkerOptions>();

            foreach (var item in lstDriver)
            {
                MarkerOptions markerOptions = new MarkerOptions().SetPosition(new LatLng(item.Lat, item.Lng));
                if (item.MaVanDon != null && item.Status == 1)
                {
                    markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.logistics_blue));
                }
                else if (item.MaVanDon == null && item.Status == 1)
                {
                    markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.logistics_yellow));
                }
                else
                {
                    markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.logistics_black));
                }
                string snippet = item.DriverName + "," + item.License + "," + item.DriverPhone + "," + item.Payload + "," + item.MaVanDon + "," + item.DienThoaiLienHe;
                markerOptions.SetSnippet(snippet);
                GMap.AddMarker(markerOptions);
                //GMap.
            }
            GMap.MarkerDragEnd += gmap_MarkerDragEnd;
            GMap.SetInfoWindowAdapter(this);
            GMap.SetOnInfoWindowClickListener(this);

            //MarkerOptions option = new MarkerOptions().SetPosition(latlng).SetTitle("Carrier");


            //GMap.AddMarker(options);
        }

        void gmap_MarkerDragEnd(object sender, GoogleMap.MarkerDragEndEventArgs e)
        {
            LatLng pos = e.Marker.Position;
            Console.WriteLine(pos.ToString());
        }
        public void OnClick(IDialogInterface dialog, int which)
        {
            dialog.Dismiss();
        }
        void goOrder(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(OrderActivity));
            StartActivity(intent);
            Finish();
        }
        void goListOrder(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(ListOrderActivity));
            StartActivity(intent);
            Finish();
        }

        private void RefreshLayout_Refresh(object sender, EventArgs e)
        {
            Task.Run(async () =>
            {
                service = new DataService(CarrierUserAppDroid.loginInfo.CurrentToken.access_token);
                var lstOrderRefresh = await service.GetListOrder();
                mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
                mRecyclerView.SetLayoutManager(mLayoutManager);

                mAdapter = new RecyclerViewAdapter(lstOrderRefresh.Result);

                mAdapter.ItemClick += Adapter_ItemClick;
                mRecyclerView.SetAdapter(mAdapter);
            });
        }

        void Adapter_ItemClick(object sender, RecyclerClickEventArgs e)
        {
            var item = mAdapter.lstData[e.Position];
            var intent = new Intent(this, typeof(DetailActivity));
            intent.PutExtra("order_detail", JsonConvert.SerializeObject(item));
            StartActivity(intent);
        }
        public View GetInfoContents(Marker marker)
        {
            throw new NotImplementedException();
        }

        public View GetInfoWindow(Marker marker)
        {
            View view = LayoutInflater.Inflate(Resource.Layout.info_driver, null, false);
            string strSnippet = marker.Snippet;
            var arrSnippet = strSnippet.Split(',');
            view.FindViewById<TextView>(Resource.Id.txtName_info).Text = arrSnippet[0];
            view.FindViewById<TextView>(Resource.Id.txtBKS_info).Text = arrSnippet[1];
            view.FindViewById<TextView>(Resource.Id.txtTaitrong_info).Text = arrSnippet[2];
            view.FindViewById<TextView>(Resource.Id.txtPhoneDriver_info).Text = arrSnippet[3];
            view.FindViewById<TextView>(Resource.Id.txtDonhang_info).Text = arrSnippet[4];
            view.FindViewById<TextView>(Resource.Id.txtPhoneUser_info).Text = arrSnippet[5];
            return view;
        }

        public void OnInfoWindowClick(Marker marker)
        {
            Console.WriteLine("Carrier");
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            var intent = new Intent();
            if (id == Resource.Id.itemOrder)
            {
                // Handle the camera action
                intent = new Intent(this, typeof(OrderActivity));
                StartActivity(intent);
                Finish();
            }
            else if (id == Resource.Id.itemHistory)
            {
                intent = new Intent(this, typeof(ListOrderActivity));
                StartActivity(intent);
                Finish();
            }
            else if (id == Resource.Id.itemPay)
            {

            }
            else if (id == Resource.Id.itemPolicy)
            {

            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);
            return true;
        }

    }
}