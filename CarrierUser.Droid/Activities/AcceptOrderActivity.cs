﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Support.V7.App;
using static Android.Views.View;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Animation;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "AcceptOrderActivity")]
    public class AcceptOrderActivity : AppCompatActivity
    {
       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Waiting_car);

            var imgView = FindViewById<ImageView>(Resource.Id.RippleOval);
            var btnimgView = FindViewById<Button>(Resource.Id.btnCancel_search);
            AnimatorSet animSet = new AnimatorSet();
            AnimatorSet animSett = new AnimatorSet();
            btnimgView.Click += delegate
            {
                Android.Animation.ObjectAnimator animatorX = Android.Animation.ObjectAnimator.OfFloat(imgView, "scaleX", 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f);
                animatorX.SetDuration(60000);
                Android.Animation.ObjectAnimator animatorY = Android.Animation.ObjectAnimator.OfFloat(imgView, "scaleY", 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f, 0, 1f);
                animatorY.SetDuration(60000);
                animSet.PlayTogether(animatorX, animatorY);
                animSet.Start();
            };    
           
        }  
    }
}