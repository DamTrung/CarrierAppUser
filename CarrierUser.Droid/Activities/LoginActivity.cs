﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidHUD;
using CarrierUser.Droid.Util;
using CarrierUserApp;
using CarrierUserApp.Droid;

namespace CarrierUser.Droid.Activities
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            DataStorage dataStorage = new DataStorage(FileAccessHelper.GetRootFolderPath());
            SetContentView(Resource.Layout.login_User);
            var btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            var txtUserName = FindViewById<TextView>(Resource.Id.txtUserName);
            var txtPass = FindViewById<TextView>(Resource.Id.txtPass);
            txtUserName.Text = "carrier";
            txtPass.Text = "123456";
            btnLogin.Click += async delegate
            {
                AndHUD.Shared.Show(this, "verifying...", -1, MaskType.Black);
                DataService dataService = new DataService(null);
                var loginInfo = await dataService.Login(txtUserName.Text, txtPass.Text);
                if (loginInfo != null)
                {
                    if (loginInfo.Message.Equals("Success"))
                    {
                        //var deviceToken = CarrierUserAppDroid.DeviceToken();
                        //if (!string.IsNullOrEmpty(deviceToken))
                        //{
                        //    dataService = new DataService(loginInfo.Result.CurrentToken.access_token);
                        //    await dataService.RegisterPushNotification(deviceToken, loginInfo.Result.Profile.ApplicationUserID);
                        //}
                        goHomeScreen();
                    }
                    else
                    {
                        Toast.MakeText(this, loginInfo.Message, ToastLength.Long).Show();
                    }
                }
                dataStorage.SaveData(loginInfo, "LoginInfo");
                CarrierUserApp.Droid.CarrierUserAppDroid.loginInfo = loginInfo.Result;
                AndHUD.Shared.Dismiss(this);
            };

            var txtviewRegister = FindViewById<TextView>(Resource.Id.textViewRegister);
            txtviewRegister.Click += register_Click;
        }

        void register_Click(object sender, EventArgs e)
        {
            Intent register = new Intent(this, typeof(AcceptOrderActivity));
            StartActivity(register);
        }
        void goHomeScreen()
        {
            //var intent = new Intent(this, typeof(OrderActivity));
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
        void goHome()
        {
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
    }
}