﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CarrierUser.Droid.Model
{
    public class ParameterPrice
    {
        public string addresssug { get; set; }
        public string addressApi { get; set; }
        public string address { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string strOrderCar { get; set; }
        public string newdistance { get; set; }
        public string origirndistance { get; set; }
        public string newtime { get; set; }
        public string origirntime { get; set; }
        public string arrayDistance { get; set; }
    }
}