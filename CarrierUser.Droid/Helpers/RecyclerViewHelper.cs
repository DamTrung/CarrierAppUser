﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using CarrierUserApp.Models;

namespace CarrierUser.Droid.Helpers
{
    public class RecyclerViewHolder : RecyclerView.ViewHolder
    {
        public TextView MaVanDon { get; set; }
        public TextView txtDate { get; set; }
        public RecyclerViewHolder(View itemView, Action<RecyclerClickEventArgs> clickListener) : base(itemView)
        {
            MaVanDon = itemView.FindViewById<TextView>(Resource.Id.order_title);
            txtDate = itemView.FindViewById<TextView>(Resource.Id.order_date);
            itemView.Click += (sender, e) => clickListener(new RecyclerClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }
    public class RecyclerViewAdapter : RecyclerView.Adapter
    {
        public event EventHandler<RecyclerClickEventArgs> ItemClick;
        public event EventHandler<RecyclerClickEventArgs> ItemLongClick;
        public List<OrderDetail> lstData = new List<OrderDetail>();
        public override int ItemCount => lstData.Count;
        public RecyclerViewAdapter(List<OrderDetail> orders)
        {
            lstData = orders;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var rHolder = holder as RecyclerViewHolder;
            var order = lstData[position];
            rHolder.MaVanDon.Text = order.MaVanDon;
            rHolder.txtDate.Text =  order.CreateAt.ToString();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater layoutInflater = LayoutInflater.FromContext(parent.Context);
            View itemView = layoutInflater.Inflate(Resource.Layout.Title, parent, false);
            var holder = new RecyclerViewHolder(itemView, OnClick);

            return holder;
        }
        protected void OnClick(RecyclerClickEventArgs args) => ItemClick?.Invoke(this, args);
        protected void OnLongClick(RecyclerClickEventArgs args) => ItemLongClick?.Invoke(this, args);

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }
    }
}