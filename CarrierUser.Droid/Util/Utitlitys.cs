﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CarrierUser.Droid.Util
{
    public static class Utitlitys
    {
        public static string StatusToString(long status)
        {
            var strResult = string.Empty;
            switch (status)
            {
                case 0:
                    {
                        strResult = "Đang khớp lệnh";
                        break;
                    }
                case 1:
                    {
                        strResult = "Đã Khớp lệnh";
                        break;
                    }
                case 2:
                    {
                        strResult = "Tài xế hủy";
                        break;
                    }
                case 3:
                    {
                        strResult = "Chủ hàng hủy";
                        break;
                    }
                case 4:
                    {
                        strResult = "Đang vận chuyển";
                        break;
                    }
                case 5:
                    {
                        strResult = "Hoàn thành";
                        break;
                    }
                case 6:
                    {
                        strResult = "Gửi tài xế lỗi";
                        break;
                    }
                case 7:
                    {
                        strResult = "Đơn hết hạn";
                        break;
                    }

                default: break;
            }
            return strResult;
        }

        public static string ConvertToCurrency(double number)
        {
            // number.ToString("N"); //12.345.00
            //return string.Format("{0:N}", number); // 12,345.00
            return (number.ToString("#,###"));
        }

        public static bool PhoneValidate(string strPhone, out string cellar)
        {
            bool isValid = false;
            cellar = string.Empty;
            string regexVina = "^(0|84)(91|94|123|125|127|129|124|88)([0-9]{7})$";
            string regexViettel = "^(0|84)(96|97|98|162|163|164|165|166|167|168|169|86)([0-9]{7})$";
            string regexMobi = "^(0|84)(90|93|122|126|128|121|120|89)([0-9]{7})$";
            string regexVietNamMobile = "^(092|0188|0186)([x0-9]{7})$";
            // Check Phone number
            if (!string.IsNullOrEmpty(strPhone))
            {
                if (Regex.IsMatch(strPhone, regexVina))
                {
                    isValid = true;
                    cellar = "Vinaphone";
                }
                else if (Regex.IsMatch(strPhone, regexViettel))
                {
                    isValid = true;
                    cellar = "Viettel";
                }
                else if (Regex.IsMatch(strPhone, regexMobi))
                {
                    isValid = true;
                    cellar = "Mobifone";
                }
                else if (Regex.IsMatch(strPhone, regexVietNamMobile))
                {
                    isValid = true;
                    cellar = "Vietnamobile";
                }
            }
            else
            {
                isValid = false;
                cellar = string.Empty;
            }
            return isValid;
        }
    }
}