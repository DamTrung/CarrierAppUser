﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Models
{
    public class Driver
    {
        public int OrderId { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public double Gia { get; set; }
        public int LoaiThanhToan { get; set; }
        public string MaVanDon { get; set; }
        public DateTime ThoiGianDi { get; set; }
        public DateTime? ThoiGianDen { get; set; }
        public string DienThoaiLienHe { get; set; }
        public int CarId { get; set; }
        public string DriverPhone { get; set; }
        public string DriverName { get; set; }
        public string Sex { get; set; }
        public int Status { get; set; }
        public int OrganizationId { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string DriverId { get; set; }
        public string License { get; set; }
        public DateTime Last_Updated { get; set; }
        public double Payload { get; set; }
        public double Width_Size { get; set; }
        public double Height_Size { get; set; }
        public double Leng_Size { get; set; }
    }

    public class DriverInfo
    {
        public int count { get; set; }
        public List<Driver> drivers { get; set; }
    }
    public class DriverInfoObj
    {
        public int count { get; set; }
        public List<object> drivers { get; set; }
    }

    public class FindDriver
    {
        public string DriverName { get; set; }
        public string Lincense { get; set; }
        public string PayLoad { get; set; }
    }

    public class FindDriverAPI : ApiResponse<FindDriver>
    {

    }
    public class DriverAPI : ApiResponse<DriverInfo>
    {
    }
    public class DriverApiObj : ApiResponse<DriverInfoObj>
    {

    }
}
