﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Models
{
    [Table("NotificationInfo")]
   public class NotificationInfo
    {
        [Column("created_At")]
        public DateTime created_At { get { return DateTime.Now; } }
        public string content{ get; set; }
        public string title { get; set; }
    }
}
