﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarrierUserApp.Models
{

    public class OrderResponse : ApiResponse<Result>
    {
    }
    public class OrderModel
    {
        public double Gia { get; set; }
        public string Note { get; set; }
        public float SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public string CarType { get; set; }
        public string ThoiGianDi { get; set; }
        public string ThoiGianDen { get; set; }
        public int LoaiThanhToan { get; set; }
        public float FromLongitude { get; set; }
        public float FromLatitude { get; set; }
        public float ToLatitude { get; set; }
        public float ToLongitude { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float Lenght { get; set; }
        public int VAT { get; set; }
        public float TrongLuong { get; set; }
        public string Phone { get; set; }
        //public string NguoiLienHe { get; set; }
        //public int ParentId { get; set; }
    }

    public class Result
    {
        public int Id { get; set; }
        public float Gia { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public double SoKmUocTinh { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public DateTime ThoiGianDi { get; set; }
        public DateTime ThoiGianDen { get; set; }
        public string UserName { get; set; }
        public int LoaiThanhToan { get; set; }
        public string MaVanDon { get; set; }
        public DateTime CreateAt { get; set; }
        public object UpdateAt { get; set; }
        public double FromLat { get; set; }
        public double FromLng { get; set; }
        public double ToLat { get; set; }
        public double ToLng { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Lenght { get; set; }
        public bool VAT { get; set; }
        public double TrongLuong { get; set; }
        public string DienThoaiLienHe { get; set; }
        public string Created_By { get; set; }
        public string NguoiLienHe { get; set; }
        public object ParentId { get; set; }
    }

    public class PriceAPIOrder : ApiResponse<double>
    {
        public double PriceAPI { get; set; }
    }
    public class ParameterGetPrice
    {
        public string addresssug { get; set; }
        public string addressApi { get; set; }
        public string address { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string strOrderCar { get; set; }
        public string newdistance { get; set; }
        public string origirndistance { get; set; }
        public string newtime { get; set; }
        public string origirntime { get; set; }
        public string arrayDistance { get; set; }
    }

    public class GetPrice
    {
        public string Version { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public double Result { get; set; }
    }


    public class OrderDetail
    {
        public int Id { get; set; }
        public object ParentId { get; set; }
        public string MaVanDon { get; set; }
        public string TenHang { get; set; }
        public string DiemDiChiTiet { get; set; }
        public string DiemDenChiTiet { get; set; }
        public DateTime? ThoiGianDi { get; set; }
        public DateTime? ThoiGianDen { get; set; }
        public double Gia { get; set; }
        public int LoaiThanhToan { get; set; }
        public DateTime CreateAt { get; set; }
        public string Created_By { get; set; }
        public int OrderStatus { get; set; }
        public int TrackingStatus { get; set; }
        public string DriverId { get; set; }
        public string DriverName { get; set; }
    }
    public class RootObjectOrder
    {
        public List<Result> Result { get; set; }
    }
    public class OrderInfoComparer : IEqualityComparer<OrderDetail>
    {
        public bool Equals(OrderDetail x, OrderDetail y)
        {
            if (x.Id == y.Id)
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(OrderDetail obj)
        {
            return obj.Id.GetHashCode();
        }
    }

    public class ListOrder
    {
        public string Version { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public List<OrderDetail> Result { get; set; }
    }
}
