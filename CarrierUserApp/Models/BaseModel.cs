﻿using System;
namespace CarrierUserApp
{
    public class ApiResponse<T>
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string Version { get; set; }
        public T Result { get; set; }
        //spublic RMSRequestResult Result { get; set; }
    }
    public class ApiRequestData
    {
        public string CredentialID { get; set; }
        public int CredentialType { get; set; }
        public object Data { get; set; }
    }
    public enum RMSRequestResult : byte
    {
        /// <summary>
        /// Describes unsupported result
        /// </summary>
        Unknown,

        /// <summary>
        /// Indicates that requestor does not have permission for requested request
        /// </summary>
        NotPermitted,

        /// <summary>
        /// Indicates that requestor does not have valid credential for requested request
        /// </summary>
        InvalidCredential,

        /// <summary>
        /// Indicates that requested request is not valid
        /// </summary>
        InvalidRequest,

        /// <summary>
        /// Indicates that requested request was performed successfully
        /// </summary>
        OK
    }
}
