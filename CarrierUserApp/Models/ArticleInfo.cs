﻿using System;
using System.Collections.Generic;
using SQLite;

namespace CarrierUserApp.Models
{
    [Table("Article")]
    public class ArticleInfo
    {
        [PrimaryKey]
        public int id { get; set; }
        public DateTime createdOn { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string shortDescription { get; set; }
        public string content { get; set; }
    }

    public class ArticleInfoComparer : IEqualityComparer<ArticleInfo>
    {
        public bool Equals(ArticleInfo x, ArticleInfo y)
        {
            if (x.id == y.id)
            {
             return true;
            }
            return false;
        }
 
        public int GetHashCode(ArticleInfo obj)
        {
            return obj.id.GetHashCode();
        }
    }

}
