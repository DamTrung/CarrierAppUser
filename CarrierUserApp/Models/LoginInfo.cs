﻿using System;
namespace CarrierUserApp.Models
{
    public class LoginInfo
    {
        public Profile Profile { get; set; }
        public object DriverInfo { get; set; }
        public CurrentToken CurrentToken { get; set; }
    }

    public class CurrentToken
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
    }
    public class LoginResponse : ApiResponse<LoginInfo>
    {
    }

    public class Profile
    {
        public int Id { get; set; }
        public string Picture { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public string ParentId { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Updated_At { get; set; }
        public int Status { get; set; }
        public DateTime LastLogin { get; set; }
        public string CMND { get; set; }
        public string FullName { get; set; }
        public string PrivateKey { get; set; }
        public string MobilePhone { get; set; }
        public string ApplicationUserID { get; set; }

    }

    //public class ApiResponseRequest<T>
    //{
    //    public int Code { get; set; }
    //    public string Message { get; set; }
    //    public string Version { get; set; }
    //    public T Result { get; set; }
    //}
}
