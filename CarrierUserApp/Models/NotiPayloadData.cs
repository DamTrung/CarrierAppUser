﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarrierUserApp
{
    public class NotiPayloadData
    {
        public Aps aps { get; set; }
        public ulong articleId { get; set; }
    }

    public class Alert
    {
        public string title { get; set; }
        public string body { get; set; }
    }

    public class Aps
    {
        public Alert alert { get; set; }
        public string sound { get; set; }
    }
}