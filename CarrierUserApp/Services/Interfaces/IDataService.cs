﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarrierUserApp.Models;
namespace CarrierUserApp
{
    public interface IDataService
    {
        Task<List<ArticleInfo>> GetArticles();
        Task<ArticleInfo> GetArticleDetail(int articleId);
        Task<LoginResponse> Login(string userName, string passWord);
        Task<string> RegisterPushNotification(string token, string accountId);
        Task<RegisterInfoApi> RegisterReg(AccountModel accountModel);
        Task<OrderResponse> AddOrder(OrderModel orderModel);
        Task<GetPrice> GetPrice(ParameterGetPrice model);
        Task<ListOrder> GetListOrder();
        Task<ApiResponse<VanDonDetail>> GetOrderDetail(int orderId);
        Task<FindDriverAPI> FindDriver(long orderId);

    }
}
