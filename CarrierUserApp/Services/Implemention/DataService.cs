﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using CarrierUserApp.Models;
using System.Net.Http.Headers;

namespace CarrierUserApp
{
    public class BaseService
    {
        public HttpClient client;
        public BaseService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"http://api.vantaitoiuu.com/");
        }
        public async Task<T> PostAsync<T, U>(string api_path, U requestData)
        {
            try
            {
                var jsonInput = JsonConvert.SerializeObject(requestData);
                var content = new StringContent(jsonInput, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PostAsync(api_path, content);
                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<T>(json);
                    return data;
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                string mss = ex.Message;
                return default(T);
            }

        }
        public async Task<T> PostAsync<T>(string uri)
        {
            var response = await client.PostAsync(uri, null);
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<T>(json);
                return data;
            }
            else
            {
                return default(T);
            }
        }
        public async Task<T> GetAsync<T>(string api_path)
        {
            try
            {
                var response = await client.GetAsync(api_path);
                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<T>(json);
                    return data;
                    //return await Task.Run(() => JsonConvert.DeserializeObject<T>(json));
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                string mess = ex.Message;
                Exception innermess = ex.InnerException;
                return default(T);
            }
        }
    }
    public static class HttpClientExtensions
    {
        public static HttpClient AddTokenToHeader(this HttpClient cl, string token)
        {
            //int timeoutSec = 90;
            //cl.Timeout = new TimeSpan(0, 0, timeoutSec);
            string contentType = "application/json";
            cl.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
            cl.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", token));
            var userAgent = "d-fens HttpClient";
            cl.DefaultRequestHeaders.Add("User-Agent", userAgent);
            return cl;
        }
    }
    public class DataService : BaseService, IDataService
    {

        public DataService(string accessToken) : base()
        {
            if (!string.IsNullOrEmpty(accessToken))
            {
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }
        }
        public async Task<List<ArticleInfo>> GetArticles()
        {
            var lstArticles = await GetAsync<List<ArticleInfo>>($"api/news");
            return lstArticles;
        }
        public async Task<ArticleInfo> GetArticleDetail(int articleId)
        {
            try
            {
                var articleInfo = await GetAsync<ArticleInfo>($"api/news/{articleId}");
                return articleInfo;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<LoginResponse> Login(string userName, string passWord)
        {
            try
            {
                var dict = new Dictionary<string, string> { { "userName", userName }, { "password", passWord } };
                var response = await PostAsync<LoginResponse, Dictionary<string, string>>($"/api/Account/SignIn", dict);
                return response;
            }
            catch
            {
                return null;
            }
        }

        public async Task<RegisterInfoApi> RegisterReg(AccountModel model)
        {
            try
            {
                var dict = new Dictionary<string, string> { { "UserName", model.UserName }, { "Password", model.Password }, { "ConfirmPassword", model.ConfirmPassword }, { "FullName", model.FullName } ,
                { "MobilePhone", model.MobilePhone } , { "Address", model.Address } };
                var response = await PostAsync<RegisterInfoApi, Dictionary<string, string>>($"/api/Account/SignUp", dict);
                return response;
            }
            catch (Exception ex)
            {
                var mss = ex.Message;
                return null;
            }
        }

        public async Task<OrderResponse> AddOrder(OrderModel m)
        {
            try
            {
                //var dict = new Dictionary<string, string> { { "Gia", m.Gia.ToString() }, { "Note", m.Note }, { "SoKmUocTinh", m.SoKmUocTinh.ToString() }, { "TenHang",m.TenHang } ,
                //{ "DiemDiChiTiet",m.DiemDiChiTiet} , { "DiemDenChiTiet", m.DiemDenChiTiet },{ "ThoiGianDi",m.ThoiGianDi},{ "ThoiGianDen",m.ThoiGianDen}, { "LoaiThanhToan",m.LoaiThanhToan.ToString() },
                //    { "FromLongitude", m.FromLng.ToString() }, {"FromLatitude",m.FromLng.ToString()},{"ToLatitude",m.ToLat.ToString()},{"ToLongitude",m.ToLng.ToString()},
                //    {"Width",m.Width.ToString() },{"Height",m.Height.ToString()},{"Lenght",m.Lenght.ToString()},{"VAT",m.VAT.ToString()},{"TrongLuong",m.TrongLuong.ToString()},{"Phone",m.DienThoaiLienHe}
                //};
                var orderbinding = new OrderModel();
                orderbinding.Gia = m.Gia;
                orderbinding.DiemDenChiTiet = m.DiemDenChiTiet;
                orderbinding.DiemDiChiTiet = m.DiemDiChiTiet;
                orderbinding.Phone = m.Phone;
                orderbinding.FromLatitude = m.FromLatitude;
                orderbinding.FromLongitude = m.FromLongitude;
                orderbinding.Height = m.Height;
                orderbinding.Lenght = m.Lenght;
                orderbinding.LoaiThanhToan = m.LoaiThanhToan;
                //orderbinding.NguoiLienHe = m.NguoiLienHe;
                orderbinding.Note = m.Note;
                orderbinding.SoKmUocTinh = m.SoKmUocTinh;
                orderbinding.TenHang = m.TenHang;
                orderbinding.ThoiGianDen = m.ThoiGianDen;
                orderbinding.ThoiGianDi = m.ThoiGianDi;
                orderbinding.ToLatitude = m.ToLatitude;
                orderbinding.ToLongitude = m.ToLongitude;
                orderbinding.VAT = m.VAT;
                orderbinding.TrongLuong = m.TrongLuong;
                orderbinding.Width = m.Width;
                orderbinding.CarType = m.CarType;
                var response = await PostAsync<OrderResponse, OrderModel>($"/api/VanDon/AddOrder", orderbinding);
                return response;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return null;
            }
        }

        public async Task<GetPrice> GetPrice(ParameterGetPrice model)
        {
            try
            {
                var dict = new Dictionary<string, string> { { "addresssug", model.addresssug }, { "addressApi", model.addressApi }, { "address", model.address }, { "lat", model.lat } ,
                { "lng", model.lng } , { "strOrderCar", model.strOrderCar },{ "newdistance", model.newdistance }, { "origirndistance", model.origirndistance }, { "newtime", model.newtime }, { "origirntime", model.origirntime }, { "arrayDistance", model.arrayDistance } };
                var response = await PostAsync<GetPrice, Dictionary<string, string>>($"/api/Orders/GetPrice", dict);
                return response;
            }
            catch (Exception)
            {
            }
            return null;
        }

        public async Task<string> RegisterPushNotification(string token, string accountId)
        {
            try
            {
                if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(accountId))
                {
                    return null;
                }
                var response = await PostAsync<string>($"/api/push-notification/device?token={token}&accountId={accountId}");
                return response;
            }
            catch
            {
                return null;
            }
        }
        public async Task<ListOrder> GetListOrder()
        {
            try
            {
                var lstOrder = await PostAsync<ListOrder>($"/api/Main/ListOrders");
                return lstOrder;
            }
            catch (Exception ex)
            {
                var mss = ex.Message;
                throw;
            }
        }
        public async Task<ApiResponse<VanDonDetail>> GetOrderDetail(int orderId)
        {
            try
            {
                var orderInfo = await GetAsync<ApiResponse<VanDonDetail>>($"api/VanDon/OrderDetail?id={orderId}");
                return orderInfo;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public async Task<DriverAPI> GetListDriver(string orderCode, string bsxCode)
        {
            try
            {
                var lstDriver = new DriverAPI();
                var uri = "api/Main/ListDrivers";
                string uriBsx, uriOrder;
                if (orderCode == "" && bsxCode != "")
                {
                    uriBsx = uri + "?bsxCode=" + bsxCode;
                }
                else if (orderCode != "" && bsxCode == "")
                {
                    uriOrder = uri + "?orderCode=" + orderCode;
                }else if (orderCode != "" && bsxCode != "")
                {
                    uriBsx = uri + "?bsxCode=" + bsxCode;
                    uriOrder = uri + "?orderCode=" + orderCode;
                    lstDriver = await PostAsync<DriverAPI>($"{uriBsx}");
                    if(lstDriver.Result.count == 0)
                    {
                        lstDriver = await PostAsync<DriverAPI>($"{orderCode}");
                    }
                }
                else
                {
                    lstDriver = await PostAsync<DriverAPI>($"{uri}");
                }
                return lstDriver;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<FindDriverAPI> FindDriver(long orderId)
        {
            try
            {
                var driverInfo = await GetAsync<FindDriverAPI>($"api/Driver/FindDriver?OrderId={orderId}");
                return driverInfo;
            }
            catch (Exception)
            {

                return null;
            }
        }
    }

}
