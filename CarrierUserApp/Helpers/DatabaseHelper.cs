﻿using System;
using System.Collections.Generic;
using System.IO;
using SQLite;
using CarrierUserApp.Models;
using System.Threading.Tasks;
using System.Linq;
using CarrierUserApp.Utils;

namespace CarrierUserApp
{
    public class DatabaseHelper
    {
        public string StatusMessage { get; set; }

        private SQLiteAsyncConnection connection;

        public static bool TableExists<T>(SQLiteConnection connection)
        {
            const string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
            var cmd = connection.CreateCommand(cmdText, typeof(T).Name);
            return cmd.ExecuteScalar<string>() != null;
        }
        public DatabaseHelper(string dbPath)
        {
            try
            {
                connection = new SQLiteAsyncConnection(dbPath);
                // Create table if need
                connection.CreateTableAsync<ArticleInfo>().ContinueWith(t => { });
                connection.CreateTableAsync<NotificationInfo>().ContinueWith(t => { });
            }
            catch (Exception ex)
            {
                StatusMessage = string.Format("Can not create database. Error: {0}", ex.Message);
            }
        }
        public async Task AddNewArticle(ArticleInfo article)
        {
            int result = 0;
            try
            {
                //basic validation to ensure a name was entered
                if (article == null)
                    throw new Exception("Valid data required");

                result = await connection.InsertAsync(article);

                StatusMessage = string.Format("{0} record(s) added [Name: {1})", result, article.id);
            }
            catch (Exception ex)
            {
                StatusMessage = string.Format("Failed to add {0}. Error: {1}", article.id, ex.Message);
            }

        }
        public async Task AddNewOrder(List<OrderDetail> order)
        {
            int result = 0;
            try
            {
                //basic validation to ensure a name was entered
                if (order == null)
                    throw new Exception("Valid data required");

                result = await connection.InsertAllAsync(order);

                StatusMessage = string.Format("{0} record(s) added)", result);
            }
            catch (Exception ex)
            {
                StatusMessage = string.Format("Failed to add {0} item. Error: {1}", order.Count, ex.Message);
            }
        }
        public async Task<List<ArticleInfo>> GetAllArticles()
        {
            try
            {
                return await connection.Table<ArticleInfo>().ToListAsync();
            }
            catch (Exception ex)
            {
                StatusMessage = string.Format("Failed to retrieve data. {0}", ex.Message);
            }

            return new List<ArticleInfo>();
        }
        public async Task<bool> AddNotification(NotificationInfo notificationInfo)
        {
            int result = 0;
            try
            {
                //basic validation to ensure a name was entered
                if (notificationInfo == null)
                    throw new Exception("Valid data required");

                result = await connection.InsertAsync(notificationInfo);

                StatusMessage = string.Format("{0} record(s) added)", result);
            }
            catch (Exception ex)
            {
                StatusMessage = string.Format("Failed to add {0} item. Error: {1}", notificationInfo.title, ex.Message);
            }
            return result == 1 ? true : false;
        }
        public async Task<List<NotificationInfo>>GetNotificationInfos(DateTime date)
        {
            try
            {
                var timeStamp = Util.ConvertToTimestamp(date);
                // var lstData = await connection.Table<NotificationInfo>().OrderByDescending(o => o.created_At).ToListAsync();
                // Use locks to avoid database collitions
             //   var data = connection.Table<NotificationInfo>();
                //var query = "Select * from NotificationInfo Where created_At >=? && created_At<=?";
                //   return query.AsEnumerable();
              //  var lstData = await data.Where(p => p.created_At >= timeStamp && p.created_At <= timeStamp).OrderByDescending(p => p.created_At).ToListAsync();
                var query = await connection.Table<NotificationInfo>().ToListAsync();

                var lstData = query.Where(p => p.created_At.Date.Equals(date.Date)).OrderByDescending(p => p.created_At).ToList();
                return lstData;
            }
            catch (Exception ex)
            {
                StatusMessage = string.Format("Failed to retrieve data. {0}", ex.Message);
            }
            return new List<NotificationInfo>();
        }
    }
}
